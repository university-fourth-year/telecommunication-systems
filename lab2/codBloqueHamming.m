function [bloque_cod] = codBloqueHamming(mensaje, G)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que calcula el código Hamming de n bits asociado a un mensaje de
% k bits, a partir del mensaje y la matriz generadora
%
%  bloque_cod = codBloqueHamming(mensaje, G)
%
%    Entradas:
%
%          mensaje : mensaje de k bits
%                G : matriz k x n que genera un bloque de n bits a partir
%                    de un mensaje de k bits
%
%    Salidas:
%
%        bloque_cod: bloque de n bits que corresponde al mensaje
%                    introducido
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cálculo del bloque codificado mediante el producto Mod2 de mensaje * G
% bloque_cod = ...

end

