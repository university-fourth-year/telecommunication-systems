function [mensaje_rec] = decBloqueHamming(bloque_recibido, Ht, err_mat)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que calcula el mensaje de k bits más probable asociado
% a un código Hamming de n bits
%
%  mensaje_rec = decBloqueHamming(bloque_recibido, Ht, err_mat)
%
%    Entradas:
%
%        bloque_recibido : bloque de n bits recibido
%                Ht : matriz n x q de comprobación de paridad
%           err_mat : matriz cuyas filas contienen los posibles patrones de
%                     error
%
%    Salidas:
%
%       mensaje_rec : mensaje de k bits más probable asociado al bloque
%                     Hamming de n bits recibido
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Cálculo del vector de síndrome asociado al bloque recibido
%%%% Recuerde que s = y * Ht (mod2)
% v_sindrome = ...

% Obtenga el índice de fila de err_mat que se corresponde
% con el vector de síndrome obtenido
% v_err_idx = ...

% Seleccione la fila de err_mat y guárdela en err
% err = ...

% Corrección del bloque recibido con el patrón de error más probable
%%%% Recuerde que bloque_pred = bloque_recibido + err (mod2)
% bloque_pred = ...

% Obtenga el mensaje contenido en el bloque predicho
%%%% Recuerde que son los k primeros bits del bloque de n bits
% mensaje_rec = ...

end

