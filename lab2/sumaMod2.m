function [suma] = sumaMod2(x1,x2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que suma Mod2 dos matrices/vectores
%
%  suma = sumaMod2(x1,x2)
%
%    Entradas:
%
%              x1 : matriz/vector 1
%              x2 : matriz/vector 2
%
%    Salidas:
%
%             suma: suma Mod2 de x1 y x2
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if size(x1) == size(x2)
    suma = logical(mod(x1 + x2, 2));
else
    error('Las matrices/vectores introducidas no tienen las mismas dimensiones');
end

end

