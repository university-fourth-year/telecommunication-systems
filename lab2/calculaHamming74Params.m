function [G, Ht, err_mat, sind_mat] = calculaHamming74Params()
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que devuelve las matrices necesarias para realizar la
% codificación y decodificación Hamming (7,4)
%
%  [G, Ht, err_mat, sind_mat] = calculaHamming74Params()
%
%    Salidas:
%
%              G: matriz generadora
%             Ht: matriz de comprobación de paridad
%        err_mat: matriz q x n cuyas filas contienen los patrones de error
%                 que pueden corregirse
%       sind_mat: matriz q x q cuyas filas contienen los posibles vectores
%                 síndrome. Esta matriz está relacionada con err_mat, de
%                 forma que el i-ésimo vector síndrome (i-ésima fila
%                 de sind_mat) corresponde al i-ésimo patrón de error
%                 (i-ésima fila de err_mat)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Definición de matriz generadora
G = [1, 0, 0, 0, 1, 0, 1;...
     0, 1, 0, 0, 1, 1, 1;...
     0, 0, 1, 0, 1, 1, 0;...
     0, 0, 0, 1, 0, 1, 1];

% Vectores de error posibles
err_mat = [0, 0, 0, 0, 0, 0, 0;...
         1, 0, 0, 0, 0, 0, 0;...
         0, 1, 0, 0, 0, 0, 0;...
         0, 0, 1, 0, 0, 0, 0;...
         0, 0, 0, 1, 0, 0, 0;...
         0, 0, 0, 0, 1, 0, 0;...
         0, 0, 0, 0, 0, 1, 0;...
         0, 0, 0, 0, 0, 0, 1];

% Definición de Ht a partir de la matriz generadora
%%%% Recuerde que Ht = [P
%%%%                   ---
%%%%                   I_q]
%%%% donde I_q es una matriz idenidad q x q, y q = n - k
% Ht = ...

% Vectores de síndrome asociados a los vectores de error
%%%% Calcule el vector de síndrome que corresponde a cada patrón de error
%%%% (i.e. fila de err_mat)
%%%% Para ello multiplique Mod2 err_mat * Ht
% sind_mat = ...

% Ordenar filas de la matriz de síndrome
%%%% Ordene las filas de la matriz de vectores síndrome en orden creciente
%%%% según su valor
for i=1:height(sind_mat)
    line=sind_mat(i,:);
    for j=i:height(sind_mat)
        line2=sind_mat(j,:);
        if (sum(line2)>sum(line))
            sind_mat(i,:)=line2;
            sind_mat(j,:)=line;
        end
    end
end
% sind_mat = ...

% Ordenar filas de la matriz de errores
%%%% Modifique la posición de las filas de err_mat para que sigan el orden
%%%% de los vectores síndrome, de forma que se mantenga la correspondencia
%%%% entre filas de sind_mat y filas de err_mat
% err_mat = ...

end

