clear variables;
close all;

Nbits = 1e5;
pe_ch = 10.^linspace(-5, -1, 30); % probabilidad de error de bit del canal

rng('default');
rng(0);

% Para obtener información sobre las funciones de MATLAB  para codificación
% convolucional consulte:
% https://www.mathworks.com/help/comm/ug/error-detection-and-correction.html?#fp7405

% Muestra más antigua que utiliza el codificador convolucional - 1, para
% varios casos distintos
clens = [3, 4, 5, 6];

% Descripción de qué entradas usa cada salidad del codificador
% convolucional
%%%% Cada fila corresponde a un valor de clens
tdescr = [6, 7;...
          15, 16;...
          35, 23;...
          51, 73];

% Profundidad de truncamiento del decodificador
tb = 21;

% Genere el mensaje original
%%%% Para ello, utilice la función genera_bits
% mensaje = ...

fprintf('Calculando pe');
pe_conv = zeros(length(pe_ch), length(clens));
for j = 1:length(clens)
    for i = 1:length(pe_ch)
        % Genere la descripción trellis del diagrama de bloques del codificador
        %%%% Para ello, utilice la función poly2trellis usando clen
%         trellis = ...

        % Obtenga el mensaje codificado que se va a transmitir por el canal
        %%%% Para ello, utilice la función convenc introduciendo el mensaje
        %%%% y la descripción trellis del codificador
        %%%% Esta función realiza la convolución mod2 a partir de los bits
        %%%% de la secuencia original según la descripción trellis introducida
%         mensaje_codificado = ...

        % Calcule el vector de errores que van a contaminar el mensaje codificado
        %%%% La frecuencia de bits erróneos debe estar marcada por pe_ch(i)
%         z = ...

        % Obtenga el mensaje recibido combinando el mensaje codificado con el
        % vector de bits erróneos
        %%%% Para ello, utilice la función sumaMod2
%         mensaje_codificado_recibido = ...

        % Decodifique el mensaje codificado recibido para obtener
        % la estimación del mensaje original
        %%%% Para ello, utilice la función vitdec en modo 'trunc', 'hard'
        %%%% Esta función utiliza el algoritmo de Viterbi para decodificar
        %%%% la secuencia recibida
%         mensaje_decodificado = ...

        % Estime la nueva probabilidad de error de bit a partir del mensaje
        % original y el decodificado
%         pe_conv(i,j) = ...
    end
    fprintf(' .');
end
fprintf('\n');

%%%% DESCOMENTAR AL TERMINAR
% Una vez pueda ejecutar el código sin errores, descomente esta sección
% para analizar las prestaciones obtenidas
% figure(1);
% plot(pe_ch, pe_conv);
% hold on;
% plot(pe_ch, pe_ch);
% hold off;
% grid on;
% xlabel('p_e del canal');
% ylabel('p_e gracias a la codificación de canal');
% 
% strs_legend = cell(length(clens) + 1, 1);
% for k = 1:length(clens)
%     strs_legend{k} = sprintf('p_e conv l=%d', clens(k));
% end
% strs_legend{end} = 'p_e canal';
% legend(strs_legend{:}, 'Location', 'northwest');