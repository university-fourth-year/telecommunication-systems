clear variables;
close all;

%HAMMING QUE ENVIA BLOQUES DE 4 EN 4 Y ENVIA 7. SOLO SE PUEDE CORREGIR
%ERRORES DE UN BIT
Nbits = 1e6;
%Nbits = 5;
pe_ch = 1e-2; % probabilidad de error de bit del canal
Nrep = 5;

% Para que la simulación sea repetible
rng('default');
rng(0);

% Genere el mensaje original
%%%% Para ello, utilice la función genera_bits
mensaje = genera_bits(Nbits);

% Obtenga el mensaje codificado que se va a transmitir por el canal
%%%% Para ello, utilice la función codMensajeRepN
mensaje_codificado = codMensajeRepN(mensaje,Nrep);

% Calcule el vector de errores que van a contaminar el mesaje codificado
%%%% La frecuencia de bits erróneos debe estar marcada por pe_ch
z = (rand(1,length(mensaje_codificado))<=pe_ch);

% Obtenga el mensaje recibido combinando el mensaje codificado con el
% vector de bits erróneos
mensaje_codificado_recibido =sumaMod2(mensaje_codificado, z);

% Decodifique el mensaje codificado recibido para obtener la estimación del
% mensaje original
%%%% Para ello, utilice la función decMensajeRepN
mensaje_decodificado = decMensajeRepN(mensaje_codificado_recibido,Nrep);

% Estime la nueva probabilidad de error de bit a partir del mensaje
% original y el decodificado
cnt=0;
for i=1:length(mensaje_decodificado)
    if mensaje_decodificado(i) ~= mensaje (i)
        cnt=cnt+1;
    end
end
pe = cnt/length(mensaje)