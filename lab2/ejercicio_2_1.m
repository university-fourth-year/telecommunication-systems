clear variables;
close all;

Nbits = 1e4;
pe_ch = 1e-2; % probabilidad de error de bit del canal

% Para que la simulación sea repetible
rng('default');
rng(0);

% Genere el mensaje original
%%%% Para ello, utilice la función genera_bits
% mensaje = ...

% Obtenga el mensaje codificado que se va a transmitir por el canal
%%%% Para ello, utilice la función codMensajeHamming74
%%%% Recuerde que debe completar antes la función codBloqueHamming
% mensaje_codificado = ...

% Calcule el vector de errores que van a contaminar el mensaje codificado
%%%% La frecuencia de bits erróneos debe estar marcada por pe_ch
% z = ...

% Obtenga el mensaje recibido combinando el mensaje codificado con el
% vector de bits erróneos
%%%% Para ello, utilice la función sumaMod2
% mensaje_codificado_recibido = ...

% Decodifique el mensaje recibido para obtener la estimación
% del mensaje original
%%%% Para ello, utilice la función decMensajeHamming74
%%%% Recuerde que debe completar antes la función decBloqueHamming
% mensaje_decodificado = ...

% Estime la nueva probabilidad de error de bit a partir del mensaje
% original y el decodificado
% pe = ...