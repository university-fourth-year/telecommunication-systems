function [mensaje_cod] = codMensajeRepN(mensaje, N)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Funcion que replica N veces cada bit del mensaje de entrada  
%
%  mensaje_cod = codMensajeRepN(mensaje, N)
%
%    Entradas:
%
%        mensaje : cadena de bits original que se quiere transmitir
%              N : número de veces que se repite cada dígito
%
%    Salidas:
%
%     mensaje_cod: cadena de bits que contiene N veces cada dígito
%                  del mensaje original
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=1:length(mensaje)
    for j=1:N
        mensaje_cod (N*(i-1)+j)=mensaje(i);
    end
end
end

