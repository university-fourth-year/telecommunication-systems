function [mensaje_dec] = decMensajeHamming74(mensaje_recibido)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que decodifica bloque a bloque según un código Hamming (7,4)
% un mensaje de longitud indeterminada.
%
% Importante: para su correcto funcionamiento necesita que la
% implementación de decBloqueHamming.m y calculaHamming74Params
% sea correcta
%
%  bloque_cod = decMensajeHamming74(mensaje)
%
%    Entradas:
%
%       mensaje_recibido : mensaje en dígitos binarios. Su longitud
%                          debe ser múltiplo de 7
%
%    Salidas:
%
%       mensaje_dec: mensaje decodificado
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nmsg = 4;
nblock = 7;

[G, Ht, err_mat, sind_mat] = calculaHamming74Params();

nblocks = length(mensaje_recibido) / nblock;
mensaje_dec = zeros(1, nblocks * nmsg, 'logical');
for i = 1:nblocks
    iblock = (i-1) * nblock + 1;
    imsg = (i-1) * nmsg + 1;
    
    bloque_cod = mensaje_recibido(iblock:iblock+nblock-1);
    
    decod_msg = decBloqueHamming(bloque_cod, Ht, err_mat);
    
    mensaje_dec(imsg:imsg+nmsg-1) = decod_msg;
end

end

