clear variables;
close all;

pe_ch = 10.^linspace(-5, -1, 20); % probabilidad de error de bit del canal

% Longitudes de bloque n
Nhamm = [3, 7, 15, 31];

% Longitudes de mensaje k
% La i-ésima longitud k se corresponde con la i-ésima longitud n de Nhamm
Khamm = [1, 4, 11, 26];

Nbits = prod(Khamm) * 1e3;

% Para que la simulación sea repetible
rng('default');
rng(0);

% Genere el mensaje original
% Para ello, utilice la función genera_bits
% mensaje = ...

fprintf('Calculando pe');
pe_hamm = zeros(length(pe_ch), length(Nhamm));
for j = 1:length(Nhamm)
    for i = 1:length(pe_ch)
        % Obtenga el mensaje codificado que se va a transmitir por el canal
        %%%% Para ello, utilice la función encode de MATLAB con Nhamm(j)
        %%%% y Khamm(j)
%         mensaje_codificado = ...
        
        % Calcule el vector de errores que van a contaminar el mensaje codificado
        %%%% La frecuencia de bits erróneos debe estar marcada por pe_ch(i)
%         z = ...

        % Obtenga el mensaje recibido combinando el mensaje codificado
        % con el vector de bits erróneos
        %%%% Para ello, utilice la función sumaMod2
%         mensaje_codificado_recibido = ...

        % Decodifique el mensaje codificado recibido para obtener
        % la estimación del mensaje original
        %%%% Para ello, utilice la función decode de MATLAB con Nhamm(j)
        %%%% y Khamm(j)
%         mensaje_decodificado = ...

        % Estime la nueva probabilidad de error de bit a partir
        % del mensaje original y el decodificado
%         pe_hamm(i,j) = ...
    end
    fprintf(' .');
end
fprintf('\n');

%%%% DESCOMENTAR AL TERMINAR
% Una vez pueda ejecutar el código sin errores, descomente esta sección
% para analizar las prestaciones obtenidas
% figure(1);
% plot(pe_ch, pe_hamm);
% hold on;
% plot(pe_ch, pe_ch);
% hold off;
% grid on;
% xlabel('p_e del canal');
% ylabel('p_e gracias a la codificación de canal');
% 
% strs_legend = cell(length(Nhamm) + 1, 1);
% for k = 1:length(Nhamm)
%     strs_legend{k} = sprintf('p_e hamm (%d,%d)', Nhamm(k), Khamm(k));
% end
% strs_legend{end} = 'p_e canal';
% legend(strs_legend{:}, 'Location', 'northwest');