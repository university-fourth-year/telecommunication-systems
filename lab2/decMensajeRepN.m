function [mensaje_decod] = decMensajeRepN(mensaje_recibido, N)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Funcion que estima un mensaje codificado con repetición de dígitos
%
%  mensaje_decod = decMensajeRepN(mensaje_recibido, N)
%
%    Entradas:
%
%        mensaje_recibido : cadena de bits recibida
%                       N : número de veces que se repite cada dígito
%
%    Salidas:
%
%            mensaje_decod: mensaje estimado, donde cada dígito es la moda
%                           de N dígitos del mensaje recibido
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calcule la longitud del mensaje una vez decodificado
%%% Recuerde que conoce el mensaje recibido y el número de veces que se
%%% repite cada bit
len_mensaje_dec = length(mensaje_recibido)/N;

% Calcule un vector de índices que indique qué bits pertenecen al mismo
% grupo, es decir, todos los bits que provienen de la repetición del mismo
% bit original deben tener el mismo índice
% grp_vec = 

% Utilice la función grpstats para calcular la decisión de cada grupo de
% bits, según los índices definidos anteriormente
%%%% Estas decisiones constituyen el mensaje decodificado
% mensaje_decod = ...
cnt=0;
for i=1:len_mensaje_dec
    for j=1:N
        if mensaje_recibido(N*(i-1)+j)==1
            cnt=cnt+1;
        end
    end
    if cnt<N/2
        mensaje_decod(i)=0;
    else
        mensaje_decod(i)=1;
    end
    cnt=0;
end
end

