function [mensaje_cod] = codMensajeHamming74(mensaje)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que codifica bloque a bloque según un código Hamming (7,4)
% un mensaje de longitud indeterminada.
%
% Importante: para su correcto funcionamiento necesita que la
% implementación de calculaHamming74Params.m y codBloqueHamming.m
% sea correcta
%
%  mensaje_cod = codMensajeHamming74(mensaje)
%
%    Entradas:
%
%          mensaje : mensaje en dígitos binarios. Su longitud debe ser
%                    múltiplo de 4
%
%    Salidas:
%
%       mensaje_cod: mensaje codificado en bloques Hamming (7,4)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nmsg = 4;
nblock = 7;

G = calculaHamming74Params();

nblocks = length(mensaje) / nmsg;
mensaje_cod = zeros(1, nblocks * nblock, 'logical');
for i = 1:nblocks
    imsg = (i-1) * nmsg + 1;
    iblock = (i-1) * nblock + 1;
    
    msg = mensaje(imsg:imsg+nmsg-1);
    bloque_cod = codBloqueHamming(msg, G);
    
    mensaje_cod(iblock:iblock+nblock-1) = bloque_cod;
end

end

