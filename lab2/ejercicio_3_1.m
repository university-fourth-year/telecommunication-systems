clear variables;
close all;

Nbits = 1e4;
pe_ch = 1e-2; % probabilidad de error de bit del canal

rng('default');
rng(0);

% Para obtener información sobre las funciones de MATLAB  para codificación
% convolucional consulte:
% https://www.mathworks.com/help/comm/ug/error-detection-and-correction.html?#fp7405

% Muestra más antigua que utiliza el codificador convolucional - 1
clen = 3;

% Descripción de qué entradas usa cada salidad del codificador
% convolucional
tdescr = [6, 7];

% Profundidad de truncamiento del decodificador
tb = 3;

% Genere el mensaje original
%%%% Para ello, utilice la función genera_bits
% mensaje = ...

% Genere la descripción trellis del codificador convolucional
%%%% Para ello, utilice la función poly2trellis usando clen y tdescr
% trellis = ...

% Obtenga el mensaje codificado que se va a transmitir por el canal
%%%% Para ello, utilice la función convenc introduciendo el mensaje y la
%%%% descripción trellis del codificador
%%%% Esta función realiza la convolución mod2 a partir de los bits de la
%%%% secuencia original según la descripción trellis introducida
% mensaje_codificado = ...

% Calcule el vector de errores que van a contaminar el mensaje codificado
%%%% La frecuencia de bits erróneos debe estar marcada por pe_ch
% z = ...

% Obtenga el mensaje recibido combinando el mensaje codificado con el
% vector de bits erróneos
%%%% Para ello, utilice la función sumaMod2
% mensaje_codificado_recibido = ...

% Decodifique el mensaje codificado recibido para obtener la estimación del
% mensaje original
%%%% Para ello, utilice la función vitdec en modo 'trunc', 'hard'
%%%% Esta función utiliza el algoritmo de Viterbi para decodificar
%%%% la secuencia recibida
% mensaje_decodificado = ...

% Estime la nueva probabilidad de error de bit a partir del mensaje
% original y el decodificado
% pe = ...