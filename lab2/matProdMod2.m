function [matprod] = matProdMod2(m1,m2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Función que multiplica Mod2 dos matrices/vectores
%
%  matprod = matprodMod2(x1,x2)
%
%    Entradas:
%
%              m1 : matriz/vector 1
%              m2 : matriz/vector 2
%
%    Salidas:
%
%             matprod: producto Mod2 de x1 y x2
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if size(m1, 2) == size(m2, 1)
    matprod = logical(mod(m1 * m2, 2));
else
    error('El número de columnas de m1 debe ser igual al número de filas de m2');
end

end

