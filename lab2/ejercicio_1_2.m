clear variables;
close all;

Nbits = 1e4;
pe_ch = 10.^linspace(-5, -1, 20); % probabilidad de error de bit del canal
size(pe_ch)
Nrep = [3, 5, 7, 9];

% Para que la simulación sea repetible
rng('default');
rng(0);

% Genere el mensaje original
%%%% Para ello, utilice la función genera_bits
mensaje = genera_bits (Nbits);

fprintf('Calculando pe');
pe_rep = zeros(length(pe_ch), length(Nrep));

for j = 1:length(Nrep)
    for i = 1:length(pe_ch)
        % Obtenga el mensaje codificado que se va a transmitir por el canal
        %%%% El número de repeticiones se obtiene de Nrep(j)
        mensaje_codificado = codMensajeRepN(mensaje,Nrep(j));
        
        % Calcule el vector de errores que van a contaminar el mensaje codificado
        %%%% La frecuencia de bits erróneos debe estar marcada por pe_ch(i)
        z = (rand(1,length (mensaje_codificado))<=pe_ch(i));

        % Obtenga el mensaje recibido combinando el mensaje codificado con el
        % vector de bits erróneos
        mensaje_codificado_recibido = sumaMod2(mensaje_codificado,z);

        % Decodifique el mensaje codificado recibido para obtener la estimación del
        % mensaje original
        %%%% El número de repeticiones se obtiene de Nrep(j)
        mensaje_decodificado = decMensajeRepN(mensaje_codificado_recibido,Nrep(j));

        % Estime la nueva probabilidad de error de bit a partir del mensaje
        % original y el decodificado

        cnt=0;
        for k=1:length(mensaje_decodificado)
            if mensaje_decodificado(k) ~= mensaje (k)
            cnt=cnt+1;
            end
        end
        pe_rep(i,j) = cnt/length(mensaje);
    end
    fprintf(' .');
end
fprintf('\n');

%%%% DESCOMENTAR AL TERMINAR
% Una vez pueda ejecutar el código sin errores, descomente esta sección
% para analizar las prestaciones obtenidas
size(pe_rep)
figure(1);
plot(pe_ch, pe_rep);
hold on;
plot(pe_ch, pe_ch);
hold off;
grid on;
xlabel('p_e del canal');
ylabel('p_e gracias a la codificación de canal');
% 
strs_legend = cell(length(Nrep) + 1, 1);
for k = 1:length(Nrep)
    strs_legend{k} = sprintf('p_e rep %d', Nrep(k));
end
strs_legend{end} = 'p_e canal';
legend(strs_legend{:}, 'Location', 'northwest');