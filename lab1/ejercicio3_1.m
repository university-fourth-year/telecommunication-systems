clear variables;
close all;

nbits = 1e6; % longitud de la secuencia de bits
npam = 4; % N-PAM

nbits_por_simb = log2(npam);
n_simbol_seq = nbits / nbits_por_simb;
vals_simbol = real(pammod(0:npam-1, npam));

% Secuencia de sÃ­mbolos
bits_tx = genera_bits(nbits);
A_tx = codifica_gray_pam(bits_tx,npam);

% EnergÃ­a media de sÃ­mbolo
EsA = mean(A_tx.^2);

% Canales
pi = cell(4, 1);
pi{1} = [1, 0];
pi{2} = [1, 1/6];
pi{3} = [1, 1/3];
pi{4} = [1, 1/2];
nch = 4;

% Matriz que contiene todas las combinaciones posibles
% sÃ­mbolo transmitido - estado del canal
A_mat = permn(vals_simbol, 2);

% Posibles salidas cada canal discreto equivalente (sin ruido) 
%alomejor faltan transpuestas
o1 = A_mat * pi{1}';
o2 = A_mat * pi{2}';
o3 = A_mat * pi{3}';
o4 = A_mat * pi{4}';

%%%% Constelaciones recibidas (sin ruido)
figure(1);
plot_symb(o2, npam, vals_simbol, 1);
xlim([min(vals_simbol) - 1, max(vals_simbol) + 1]);

figure(2);
plot_symb(o2, npam, A_mat(:,1)', 1);

figure(3);
plot_symb(o3, npam, A_mat(:,1)', 1);

figure(4);
plot_symb(o4, npam, A_mat(:,1)', 1);

%%%% GrÃ¡fica de la probabilidad de error en funciÃ³n de la SNR
figure(5);
snr_range = 0:2:20;
pe_ij = zeros(nch, length(snr_range));


for i = 1:nch
    % Señal observada en recepción (sin ruido)
    oi = A_mat.*pi{i}; %para barrer los canales
    Esoi = (mean(abs(A_mat))).^2 ;
    for j = 1:length(snr_range)
        % Señal de ruido
        mu = 0;
        N0 = EsA/(2*10^(snr_range(j)/10));
        sigma = sqrt(N0/2);
        z = normrnd(mu, sigma, n_simbol_seq,1)';
        
        % Secuencia recibida
        q = filter(pi{i},1,A_tx) + z;
        
        % Secuencia de sÃ­mbolos decidida
        A_dec = decisor_pam(q,npam);

        % Secuencia de bits recibida
        bits_out = decodifica_gray_pam(A_dec,npam);

        fprintf('Number of errors = %d\n', sum(xor(bits_tx, bits_out)));
        
        % Probabilidad de error
         pe_ij(i,j) = max((sum(xor(bits_tx,bits_out))/nbits),1/nbits);%... sumatorio de la diferencia entre los bits
        % transmitidos y los de salida (usar XOR) y dividirlo entre el n de
        % bits, pero haciendo el máximo entre eso y 1/nbits. Si no, va a
        % quedar la función cortada.
        % sumatorio y dibidir entre numero de bits //// bits erroneos /bits
        % totales
        % hacer el maximo entre eso y 1/nbits
    end
end

 semilogy(snr_range, pe_ij);
 grid on;
 xlabel('SNR (dB)');
 ylabel('p_e');
 title('p_e en función de la SNR para los diferentes canales');
 legend('p_1=\delta[n]', 'p_2=\delta[n]+1/6\delta[n-1]',...
    'p_3=\delta[n]+1/3\delta[n-1]', 'p_4=\delta[n]+1/2\delta[n-1]');