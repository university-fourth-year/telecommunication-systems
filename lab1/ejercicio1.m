clear variables;
close all;

nbits = 1e3; % longitud de la secuencia de bits
npam = 4; % N-PAM
SNRdB = 15;

nbits_por_simb = log2(npam);
n_symbol_seq = nbits / nbits_por_simb;

 %Secuencia de bits (entrada del transmisor)
 bits_tx = genera_bits(nbits)

 %Secuencia de símbolos (salida del codificador)
 A_tx = codifica_gray_pam(bits_tx,npam)

 %Energía media de símbolo
 EsA = mean(A_tx.^2)

% Señal de ruido
% mu = ...
% sigma = ...
% z = ...

% Secuencia recibida
% q = ...

% Diagrama de dispersión
% plot_symb(q, npam, A_tx, 1);

