clear variables;
close all;

nbits = 1e3; % longitud de la secuencia de bits
npam = 4; % N-PAM
SNRdB = 20;

nbits_por_simb = log2(npam);
n_symbol_seq = nbits / nbits_por_simb;
vals_simbol = real(pammod(0:npam-1, npam));

% Secuencia de bits (entrada del transmisor)
 bits_tx = genera_bits(nbits);

% Secuencia de símbolos (salida del codificador)
 A_tx = codifica_gray_pam(bits_tx,npam);

% Energía media de símbolo
 EsA = mean (A_tx.^2)

% Canal discreto equivalente
 pi = [1, 1/6];

% Matriz que contiene todas las combinaciones posibles
% símbolo transmitido - estado del canal
 A_mat = permn(vals_simbol, length(pi));

% Posibles salidas del canal discreto equivalente (sin ruido) producto de a
% mac por pi convolucionado con ?
 oi = A_mat * pi'

% Constelación recibida (sin ruido)
 figure(1);
 plot_symb(oi, npam, A_mat(:,1), 1);

% Energía media de símbolo recibido (sin ruido) mismo calcula que con la
% energía de la señal, pero con los símbolos recibidos (oi)
% Esoi = ...

% Señal de ruido%
% mu = ...0
% sigma = ...como antes
% z = ...como antes

% Secuencia recibida filter(pi,1,atx)+z
% q = 

% Diagrama de dispersión
% figure(2);
% plot_symb(q, npam, A_tx, 1);




