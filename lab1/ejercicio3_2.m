clear variables;
close all;

nbits = 1e6; % longitud de la secuencia de bits
npam = 4; % N-PAM
tblen = 11; % Profundidad de truncamiento

nbits_por_simb = log2(npam);
n_simbol_seq = nbits / nbits_por_simb;
vals_simbol = real(pammod(0:npam-1, npam));

% Secuencia de símbolos
bits_tx = genera_bits(nbits);
A_tx = codifica_gray_pam(bits_tx,npam);

EsA = mean(A_tx.^2);

% Canales
p1 = [1];
pi = [1, 1/2];
nch = 2;

figure(1);
snr_range = 0:2:20;
pe_ss = zeros(1, length(snr_range));
pe_ss_isi = zeros(1, length(snr_range));
pe_vit_isi = zeros(1, length(snr_range));
for j = 1:length(snr_range)
    % Señal de ruido
     mu = 0
     N0 = EsA/(2*10^(snr_range(j)/10));
     sigma = sqrt(N0/2);
     z = normrnd(mu, sigma, n_simbol_seq,1)';
    
    % Secuencia de observaciones recibida (canal sin ISI)
     q = filter(pi,1,A_tx) + z;
    
    % Secuencia de observaciones recibida (canal con ISI)pi es el canal
    % ideal
     q_isi0 = filter(pi,1,A_tx);
     Eso = min(abs(q_isi0).^2); %Chequear esto
     N01 = Eso/(2*10^(snr_range(j)/10)); %Chequear esto
     sigma1 = sqrt(N0/2);
     z1 = norm (sigma1);
    
     q_isi = q_isi0+z1;
    %%%% Secuencia de símbolos decidida (canal sin ISI)
    % Decisor símbolo a símbolo
    A_dec_ss = decisor_pam(q,npam);
    
    %%%% Secuencia de símbolos decidida (canal con ISI)
    % Decisor símbolo a símbolo
    A_dec_ss_isi = decisor_pam(q_isi, npam);
    
    % MLSE
     A_dec_vit_isi = mlseeq (q_isi, pi, vals_simbol,tblen,'rst');
    
    
    % Secuencia de bits recibida (canal sin ISI)
     bits_out_ss = decodifica_gray_pam(A_dec_ss,npam);

    % Secuencia de bits recibida (canal con ISI)
     bits_out_ss_isi = decodifica_gray_pam(A_dec_ss_isi,npam);
     bits_out_vit_isi = decodifica_gray_pam(A_dec_vit_isi,npam);

     fprintf('Number of errors:  ss = %d, vit = %d\n', sum(xor(bits_tx, bits_out_ss_isi)), sum(xor(bits_tx, bits_out_vit_isi)));

     pe_ss(j) = max((sum(xor(bits_tx,bits_out_ss))/nbits),1/nbits);
     pe_ss_isi(j) = max((sum(xor(bits_tx,bits_out_ss_isi))/nbits),1/nbits);
     pe_vit_isi(j) = max((sum(xor(bits_tx,bits_out_vit_isi))/nbits),1/nbits);
end

 semilogy(snr_range, cat(1,pe_ss, pe_ss_isi, pe_vit_isi));
 grid on;
 xlabel('SNR (dB)');
 ylabel('p_e');
 title('Comparación entre decisor símbolo a símbolo y decisor MLSE');
 legend('p_1=\delta[n]', 'pi_{ss}', 'pi_{vit}');





