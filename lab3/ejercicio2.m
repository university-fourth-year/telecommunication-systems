                                                                                                                                                                                                                                                                                                              
% Aquí se desarrolla el cálculo del waterfilling continuo, y se
% ilustran las diferencias con el discreto
% 1) Se especifica cómo representar canales continuos en el dominio de la
% frecuencia
% 2) Se ilustra como considerar las atenuaciones de canal a través del
% concepto de "densidad de ruido EQUIVALENTE"
% 3) Se ilustran las relaciones entre la capacidad en bits por símbolo y
% bits por segundo
% Por último se ilustra el algoritmo de Waterfilling en versión discreta y
% continua y sus diferencias.


clear all
clc

% Potencia de señal transmitida a unidades naturales
% Ps=         % dBm
ps=[3,3];
pstot=6;       % Función unidades logarítimicas a naturales


% Especificacion de  los canales: 
% Anchos de banda en Hz
 B=[3*10^3,1*10^3];        % Vector de BW en Hz
 tB=sum(B);           % Ancho de banda total empleado en el sistema en Hz

% Densidad espectral de ruido en dBm/Hz, N0
% dpN=          % Vector densidad de potencia de ruido en unidades
% logarítmicas
 dpn=[1,4]./B;          % Densidad de potencia de ruido en unidades naturales 

% Atenuacion en dB% 
% A=            % Vector atenuación en unidades logarítmicas 
 a=1;            % Atenuación en unidades naturales

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ruido equivalente por canal: en mW/Hz
 dpne=dpn*a;         % Dep de ruido equivalente teniendo en cuenta la aportación
% del ruido y de la atenuación.
 pne=dpne.*B;          % Potencia de ruido equivalente 

N= length(B);% Número de canales

% Densidad de potencia de señal uniforme
dpsu=((pstot/N)*ones(1,N))./B;         % Dep de la señal si fuera uniforme
psu=dpsu.*B;        % Potencia de señal uniforme

% SNR en los diferentes canales con distribucion de señal uniforme:
 snr=psu./pne;          % SNR en unidades naturales
 SNR= n2dB(snr);         % SNR de unidades naturales a logarítmicas 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


bpsimtot=0;
for i=1:N
 fprintf('Canal %i) ',i)
 fprintf('la SNR=%d ',snr(i))
%bpsim=c1f(dpsu(i),dpne(i),B(i))/2*B(i); %c1f es para calcular C(bps)=B*log2(1+(P/(dpn*B))). Como queremos la C en bits/símbolo, necesitamos C=0.5*log2(1+(p/(dpn*B))), por lo que dividimos lo obtenido con c1f entre 2*B para que se nos quede en bits/simbolo
 fprintf('la capacidad uniforme es %d bits por simbolo \n',C1d(psu(i),pne(i))) % Función
% capacidad de canal
 fprintf('  la capacidad uniforme es %d b/simb* R simb/seg \n',C1c(psu(i),dpne(i),B(i))/C1d(psu(i),pne(i))) % Función
% capacidad de canal. Como Rs=Rb/m, dividimos lo obtenido en c1c(bps) entre el
% número de bits por símbolo (c1d) para obtener el número de símbolos/seg
 fprintf('  la capacidad uniforme es %d bps \n\n',C1f(dpsu(i),dpne(i),B(i))) % Función capacidad de
% canal en bps
%bpsimtot=bpsimtot+bpsim;
end

 fprintf('Cuidado con sumar las capacidades por símbolo y multiplicar por  el ancho de banda total\n')
 fprintf('\n       La capacidad total NO es %d bps \n\n',Cd(psu,pne)) % Función
% capacidad de canal como suma de capacidades de canales 


 fprintf('Hay que sumar las capacidades por segundo de cada canal:\n')
 fprintf('\n       La capacidad total es %d bps \n',Cf(dpsu,dpne,B)) % Función capacidad 
% de todos los canales  
%fprintf('\n       La capacidad total es %d bps \n',) % Función capacidad 
% total de varios canales ¿¿¿Varios canales???


fprintf('\n Aquí se ve la diferencia entre el cálculo de la capacidad con canales continuos y discretos\n')
fprintf('Si asumimos canales discretos:\n')
 p_opt= wfd(pstot,dpne.*B)           % Función wfd
 dp_implicadas=p_opt./B;    % Densidades de potencia relacionadas con p_opt
fprintf('         La capacidad total resultante es %d bps \n',Cd(p_opt,dpne.*B)) % Función
%capacidad de varios canales discretos
fprintf('que claramente no es óptima \n\n')

fprintf('Si asumimos canales continuos:\n')
 dp_opt= cwfd(pstot,dpne,B)                 % Función cwfd
 ps_implicadas= dp_opt.*B          % Potencias relacionadas con dp_opt 
 fprintf('         La capacidad total maxima es %d bps \n',Cc(ps_implicadas,dpne,B)) % Función
% caacidad de varios canales continuos








%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Funciones para Waterfilling

function p=wfd(ptot,ruido) % Waterfilling discreto
% Devuelve las potencias optimas de asignacion
% Utiliza el algoritmo wfill de G. Levin que se puede bajar gratuitamente en Mathworks 
% y es muy simple de entender. 

nu=wfill(ruido,ptot,10^-3)

N=length(ruido);           % Número de canales
for i=1:N
    p(i)=nu-ruido(i);                   % Las potencias de señal deben salir mayores que 0
end

end

function dp=cwfd(ps,dn,B) % Waterfilling continuo
% Devuelve las potencias optimas de asignacion
% Utiliza el algoritmo wfill de G. Levin que se puede bajar gratuitamente en Mathworks 
% y es muy simple de entender. 

%ps: Potencia total disponible
%dn: Densidad espectral de ruido EQUIVALENTE (hay que pasarla ya
%multiplicada por la atenuación)
%B: vector de anchos de banda
dn
 nu= cwfill(dn,B,ps,10^-3)

N= length(B);
for i=1:N
   dp(i)= max(nu- dn(i),0); % la densidad de potencia debe ser mayor o igual a 0
   % o sería dp(i)=nu-dn(i)*B(i)??? (no tendría sentido creo yo, si quieres
   % dp, si restas algo a una constante ese algo también tiene que ser dp)
end


end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Funciones de capacidad de canal


function c=C1d(p,n) %capacidad en bits/simbolo
    c=(1/2)*log2(1+p/n);   % Como en el caso de la capacidad de canal discreto
end

%Simbolo= muestra; Teorema de Nyquist => B ancho de banda 2*B muestas por
%segundo: c=1/2*log2(1+p/n) * 2 B= B*log2(1+p/(dn*B))

function c=C1c(p,dn,B) %capacidad en bits/seg
    %dn SIEMPRE EQUIVALENTE
      c=B*log2(1+(p/(dn*B)));  % Capacidad de canal continuo
end


function c=C1f(dp,dn,B) %capacidad en bits/seg
    %dn SIEMPRE EQUIVALENTE
    c=  B*log2(1+dp/dn);% Capacidad de canal continuo 
 %si p=dp*B(?), log2(1+p/(dn*B))=log2((dp*B)/(dn*B))=log2(dp/dn)
end


function Ct=Cd(p,n) %capacidad total de varios canales discretos
N=length(p);    % Número de canales de potencia
 N1=length(n);   % Número de canales de ruido
 Ct=0;
if N ~=N1
    fprintf('p y n tienen que tener la misma longitud')
    return
end

for i=1:N
    c(i)= C1d(p(i),n(i));  % Función para el cálculo de una capacidad por canal
Ct=Ct+c(i); % Capacidad total como la suma de las capacidades por canal
end
end


function Ct=Cc(p,dn,B) %capacidad total de varios canales en bps.
 N= length(p);  % número de canales de potencia
 N1= length(dn);  % número de canales con densidad espectral de ruido
Ct=0;
 if N ~=N1
    fprintf('B y dn tienen que tener la misma longitud!')
    return
end

for i=1:N
     c(i)=  C1c(p(i),dn(i),B(i)); % Función cic
     Ct=Ct+c(i); % Suma de todos los canales
end  
end


function C=Cf(dp,dn,B) %capacidad total de varios canales en bps.
 N= length(dp); % número de canales con densidad espectral de potencia
 N1= length(dn); % número de canales con densidad espectral de ruido
 C=0;
 if N ~=N1
    fprintf('B y dn tienen que tener la misma longitud!')
    return
end

for i=1:N
   c(i)= C1f (dp(i), dn(i),B(i));% Función C1f
   C=C+c(i); % capacidad total
end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Funciones auxiliares

function db=n2dB(x)   % De unidades naturales a logarítmicas
N=length(x);

for i=1:N
    db(i)= 10*log10(x(i));
end

end

function n=dB2n(x)   % De unidades logarítmicas a naturales
N=length(x);

for i=1:N
     n(i)= power (10,x(i)/10);
end
end
