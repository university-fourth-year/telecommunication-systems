function nu=cwfill(dn, B, ps, tol)
%  Adaptacion del algoritmo de  G. Levin, May, 2003 para canales continuos
% dn: densidad de ruido EQUIVALENTE  en mW/Hz
% B: vector con los anchos de banda en Hz
% Pt: potencia total disponible
% References:
%   T. M. Cover and J. A. Thomas, "Elements of Information Theory", John Wiley & Sons,
%   Inc, 1991.

N=length(B);
BW=sum(B);

%first step of water filling
nu=min(dn.*B)+ps/N; %initial waterline
ptot=sum(max((nu-dn).*B,0)); %total power for current waterline
 
%gradual water filling
while abs(ps-ptot)>tol
    nu=nu+(ps-ptot)/BW;
    ptot=sum(max((nu-dn).*B,0));
end

