function [A,N] = adsl(f,d,tipo)
% f= frecuencia en Hz
% d= distancia en Km
% tipo de canal: tipo=1 canal con NEXT dominante
%                tipo=2 canal con NEXT no dominante
%Devuelve la atenuación y el ruido en un canal ADSL
% Se supone un NEXT-Dominated Environment
% Para el modelo, se sigue a Jean-Jacques Werner: "The HDSL Environment",
% IEEE JOURNAL ON SELECTED AREAS IN COMMUNICATIONS, VOL. 9, NO. 6. AUGUST 1991
%Para el ajuste de parámetros, el cable 24 AWG:
%Krista S. Jacobsen: "Providing the Right Solution for VDSL",
%Texas Instruments White Paper, 1999.

A=atenuacion_adsl(f,d);% en unidades naturales.
N=pdf_next(f,tipo); %dBm/Hz

end


function A=atenuacion_adsl(f,d) %en unidades naturales.
%f se da en Hz y se pasa a KHz
a=0.85;
b=-0.8;

A=(a*sqrt(f*1e-3)+b)*d;

end


function Noise=pdf_next(f,tipo)
a=-176.8;
Noise=3/2*10*log10(f)+a; % dBm/Hz


if tipo==2 & f> 1e6
    a=(85-140)/4e6;
    b=-140-a*5e6;
    Noise= a*f+b;
end

if tipo==2 & f> 5e6
    Noise=-140;
end

end





function n=dB2n(x)
N=length(x);

for i=1:N
    n(i)=10^(x(i)/10);
end
end
