% Se desarrolla aqui una aplicación pseudo-realista para estimar la
% capacidad máxima de un canal ADSL

% 1) Se modela el perfil de atenuación en un cable y la densidad espectral
% de ruido, considerando el NEXT como dominante. En otros casos, el perfil
% de este ruido es demasiado restrictivo a partir de frecuencias de 1MHz.
% El parámetro "tipo=2" selecciona éste último, para solo NEXT, poner
% tipo=1.

% Se consideran los parámetros de Very High Bitrate DSL como aparecen en la
% trasparencia: BW=30MHz y 4096 portadoras. Se considera que cada portadora
% tiene el mismo ancho de banda:

% 2) Se estudia la variación de la capacidad con la distancia 



clear all
clc


Nc=4096;
BWT=30e6; % Hz.

BW= BWT/Nc;   % BW por canal
ro=0.9;    % Se estima que la separación entre canales es de un 10%
B=BW*ones(1,Nc)*ro; % Anchos de banda en cada canal, se estima un factor 
% de uso 

% Frecuencias portadoras
for i=1:Nc
    f(i)=BW/2+BW*(i-1);
end

% Distancia en Km
d=1;

% Tipo de canal:
tipo=1; % NEXT predominante; el otro es 2.

% Atenuación y densidad espectral de ruido en dBm/Hz
% y ruido equivalente en dBm/Hz

for i=1:Nc
    [At(i),N(i)]=adsl(f(i),d,tipo); % Función ADSL
    Nequi(i)= dB2n(N(i))*dB2n(At(i));      % Ruido equivalente: ruido y atenuación
end
dpne= dB2n(Nequi)./B; % Densidad espectral de potencia de ruido equivalente 
% en unidades naturales


%Potencia de señal transmitida %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Ps=10;       % dBm; máximo real en 15 dBm.
ps= dB2n(Ps);% Función unidadws logarítmicas a naturales

% Densidad de potencia de señal de distribución uniforme
tB= sum(B);  % Ancho de banda total
dpsu=ps/tB *ones(1,Nc);  % Densidad espectral de potencia de la señal 
% de distribución uniforme
psu= dpsu.*tB;  % Potencia de la señal de distribución uniforme

fprintf('Con distribución uniforme, la potencia por portadora es de %d dBm\n',n2dB(psu(1)))
C_uni=Cf (dpsu,dpne,B) % Capacidad total con distribución uniforme. Función Cf.

fprintf('Con distribución uniforme,la Capacidad del sistema es de %d Mbps\n',C_uni*1e-6)

% Cálculo de la distribución óptima de potencia con Waterfilling:
 dp_opt=cwfd(ps,dpne,B);  % Densidad de potencia de señal óptima con Waterfilling. Función cwfd.

C_opt= Cf(dp_opt,dpne,B); % Capacidad de canal óptima total. Función cf.

fprintf('Con distribución óptima,la Capacidad del sistema es de %d Mbps\n',C_opt*1e-6)

ps_opt= dp_opt*BW; % Potencia de la señal con densidad de potencia óptima

figure(1)
plot(f*1e-6,ps_opt)
grid minor
title('Canales activos, a d Km')
xlabel('f (MHz)')
ylabel('Potencia asignada (mW)')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Estudio de la capacidad con la distancia

dv=0.1:0.1:10; % Km
dvl=length(dv); % Longitud de dvl
tipo=2; % 1 ó 2 de NEXT

for i=1:dvl
    % atenuación y densidad espectral de ruido en dBm/Hz
    % y ruido equivalente en dBm/Hz
    d=dv(i);
    
    for j=1:Nc
        [At(j),N(j)]= adsl(f(j),d,tipo); % Función ADSL 
        Nequi(j)= N(j)+At(j); % Canal equivalente con ruido y atenuación
    end
    
    dpne= dB2n(Nequi);% Densidad de potencia de ruido en unidades 
    % logarítmicas a naturales

    % Cálculo de la distribución óptima de potencia con Waterfilling:
    dp_opt= cwfd(ps,dpne,B);  % Función cwfd óptima

     C_opt(i)= Cf(dp_opt,dpne,B);  % Capacidad total con distribución óptima. Función cf
     C_uni(i)= Cf(dpsu,dpne,B);  % Capacidad total con distribución uniforme. Función cf
    
     ps_opt= dp_opt.*B;  % Potencia de señal con distribución óptima
    Nc_activos(i)=sum(ps_opt>ps*1e-4)/Nc*100; % Número de canales activos en porcentaje

  
    
end

figure(2)
plot(dv,C_opt,dv,C_uni)
grid minor
title('Variación de la capacidad con la distancia')
xlabel('d (Km)')
ylabel('Capacidad (Mbps)')
legend('Opt','Uni')


figure(3)
plot(dv,Nc_activos)
grid minor
title('Porcentaje de canales activos con la distancia')
xlabel('d (Km)')
ylabel('canales activos (% sobre 4096)')




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Funciones para Waterfilling

function dp=cwfd(ps,dn,B) % Waterfilling continuo
% Devuelve las potencias optimas de asignacion
% Utiliza el algoritmo wfill de G. Levin que se puede bajar gratuitamente en Mathworks 
% y es muy simple de entender. 

BW=sum(B);   % Ancho de banda total

nu=cwfill(dn, B, ps,ps/BW*1e-3);

N=length(B);   % Número de canales
for i=1:N
    dp(i)=max(nu-dn(i),0);   % Densidad de potencia de señal con distribución óptima    
end

end





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Funciones de capacidad de canal %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function c=C1f(dp,dn,B) % Capacidad de un canal con densidad de potencia y ruido, en bps
    c=B*log2(1+dp/dn);% Capacidad de canal continuo 
end



function C=Cf(dp,dn,B) % Capacidad total con densidades de potencia y ruido, en bps.
N= length(dp); % número de canales con densidad espectral de potencia
 N1= length(dn); % número de canales con densidad espectral de ruido
 if N ~=N1
    fprintf('B y dn tienen que tener la misma longitud!')
    return
end

for i=1:N
   c(i)= C1f (dp(i), dn(i),B(i));% Función C1f
end
C=sum(c);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%Funciones auxiliares %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function db=n2dB(x)   % De unidades naturales a logarítmicas
N=length(x);

for i=1:N
    db(i)= 10*log10(x(i));
end

end


function n=dB2n(x)   % De unidades logarítmicas a naturales
N=length(x);

for i=1:N
     n(i)= power (10,x(i)/10);
end
end


