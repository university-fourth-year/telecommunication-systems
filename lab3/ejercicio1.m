% La potencia de señal y de ruido tienen que estar todas expresadas en
% unidades naturales, en W o mW, por ejemplo (pero no en dBW o dBm).


clear all
clc


RL=false; % RL es Representación Logarítmica

% Especificación de potencias para los canales:
if RL   %Potencias en representacion logaritmica
   P=4     % dBm
   pot=  10*log10(P);  % Función conversión dBm a mw
else %Potencias en representacion natural mW
    pot=[3,3];    % mw
end
ptot= sum(pot);      % Potencia total
ctot=0;

% Especificación de ruido para los canales:
if RL   %Potencias en representacion logaritmica
    %R=        % dBm; 
    %ruido=     % Función conversión dBm a mw
else %Potencias en representacion natural mW
    ruido= [1e-4,4];   % mW
end


N=length(ruido);            % Número de canales


for i=1:N
 fprintf('Canal %i) ',i)
 fprintf('la snr=%d ',(pot(i)/N)/ruido(i))
 c(i)=C1d(pot(i),ruido(i));
 ctot=ctot+c(i);
 fprintf('la capacidad es %d bits \n', c(i)) % Función de
 %cálculo de la capacidad de un canal
end
fprintf('\n       La capacidad total es %d bits \n',ctot)%
% Función de cálculo de la capacidad total 

p_opt= wfd(ptot,ruido)   % Función waterfilling wfd

fprintf('         La capacidad total maxima es %d bits \n', Cd(p_opt,ruido))  % Función
% de cálculo de la capacidad total











%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Funciones para Waterfilling

function p=wfd(ptot,ruido) % Waterfilling discreto
% Devuelve las potencias óptimas de asignación para cada canal.
% Utiliza el algoritmo wfill de G. Levin que se puede descargar 
% gratuitamente de Mathworks y es muy simple de entender. 

nu=wfill(ruido,ptot,10^-3)

N=length(ruido);           % Número de canales
for i=1:N
    if ruido(i)<=nu
            p(i)=nu-ruido(i);              % Las potencias de señal deben salir mayores
    else
        p(i)=0;
    end
                              % que 0
end

end


% Funciones de capacidad de canal

function c=C1d(p,n) % Capacidad de un canal en bits
 c=(1/2)*log2(1+p/n);
end

function Ct=Cd(p,n) % Capacidad total de varios canales
 N=length(p);    % Número de canales de potencia
 N1=length(n);   % Número de canales de ruido
 Ct=0;
 if N ~=N1
    fprintf('p y n tienen que tener la misma longitud')
    return
end

for i=1:N
    c(i)= C1d(p(i),n(i));  % Función para el cálculo de una capacidad por canal
    Ct=Ct+c(i); % Capacidad total como la suma de las capacidades por canal
end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Funciones auxiliares

function db=n2dB(x)  % Cambio de unidades naturales a logarítimicas
N=length(x);

for i=1:N
   % db(i)=          % Vector con componentes en unidades logarítmicas
end

end

function n=dB2n(x)   % Cambio de unidades logarítmicas a naturales
N=length(x);

for i=1:N
    % n(i)=          % Vector con componentes en unidades naturales
end
end